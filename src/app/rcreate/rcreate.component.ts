import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from '../data.service';
import { User } from '../Models/user.model';
import { UserService } from '../user.service';
import {TranslateService} from '@ngx-translate/core';


interface Contact {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-rcreate',
  templateUrl: './rcreate.component.html',
  styleUrls: ['./rcreate.component.css']
})
export class RcreateComponent implements OnInit {
  lastIndex: Number;
  users: User[] = [];
  createForm: FormGroup;
  contactnumbers: FormArray;
  id: number = 1;
  checkContactType: string;
  formInvalidSubmit:boolean;

  param = {value: 'world'};

  constructor(private _dataService: DataService, private _fb: FormBuilder,
    private _snackBar: MatSnackBar, private _userService: UserService,private translate: TranslateService) {
      // translate.setDefaultLang('en');
        
     }

  ngOnInit(): void {
    
    this.intializeForm()
    this.getUsersData()
    //To maintain add remove icons for last input values
    this.contactnumbers = this.createForm.get('contactnumbers') as FormArray;
    this.lastIndex = this.contactnumbers.value.length - 1
       

  }

  contactTypes: Contact[] = [
    { value: 'mobile', viewValue: 'Mobile' },
    { value: 'landline', viewValue: 'Landline' },
  ];

  intializeForm() {
    this.createForm = this._fb.group({
      favourite: [false],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      gender: ['', Validators.required],
      martialstatus: [null, Validators.required],
      id: [''],
      contactnumbers: this._fb.array([
        this.createItem()
      ])


    })
  }

  createItem(): FormGroup {
    return this._fb.group({
      contact: ['', Validators.required],
      number: ['',Validators.required]
    });

  }

  switchLanguage(language: string) {
    console.log("lang",language);
    this.translate.use(language);
  }

  getUsersData() {
    this._userService.getUsersList().subscribe((response) => {
      console.log("get data",response);
      this.users = response

    })
  }


  createUser(createForm, formDirective) {
    if (createForm.valid) {
      console.log("users",this.users);
      if (this.users == undefined || this.users.length == 0) {
        this.users=[]
        createForm.patchValue({
          id: this.id
        })
      }
      else {
        // this.users = JSON.parse(localStorage.getItem('users'))
        var LastId = this.users[this.users.length - 1]['id']
        createForm.patchValue({
          id: LastId + 1,
        })
      }
      // console.log(this.users);
      this.users.push(createForm.value)
      // console.log(this.users);
      // this._userService.createUsersList(this.users)
      // localStorage.setItem('users', JSON.stringify(this.users));
      this._snackBar.open("user Created Succesfully", "close", { duration: 3000 });
      this._dataService.setRefreshData(true)
      this.formInvalidSubmit=false
      this.resetForm()
      formDirective.resetForm()
    }
    else {
      this.formInvalidSubmit=true
      this._snackBar.open("Please fill all the Details", "close", { duration: 3000 });
    }
  }



  addContactRow() {
    this.contactnumbers = this.createForm.get('contactnumbers') as FormArray;
    this.contactnumbers.push(this.createItem());
    this.lastIndex = this.contactnumbers.value.length - 1
  }

  removeContactRow(i) {
    this.contactnumbers.removeAt(i);
    this.lastIndex = this.contactnumbers.value.length - 1
  }

  // get contactNumbers()
  // {
  //   return this.createForm.get('contactnumbers') as FormArray;
  // }


  ChangeContactTypeValidation(i) {
    console.log(
      this.createForm.get('contactnumbers')['controls'][i]['controls']['contact'].value
    );
    
    this.checkContactType = this.createForm.get('contactnumbers')['controls'][i]['controls']['contact'].value
    console.log(this.checkContactType);
  
    if (this.checkContactType == "mobile") {      
      this.createForm.get('contactnumbers')['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')])
      this.createForm.get('contactnumbers')['controls'][i]['controls']['number'].updateValueAndValidity();
    } else if (this.checkContactType == "landline") {
      // this.createForm.get('contactnumbers')['controls'][i]['controls']['number']
      //   .clearValidators()
      this.createForm.get('contactnumbers')['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.minLength(11)])
      this.createForm.get('contactnumbers')['controls'][i]['controls']['number'].updateValueAndValidity();
    }

  }

  resetForm() {
    this.createForm.reset();
    this.ngOnInit()


  }



}
