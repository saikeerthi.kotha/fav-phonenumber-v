import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RcreateComponent } from './rcreate.component';

describe('RcreateComponent', () => {
  let component: RcreateComponent;
  let fixture: ComponentFixture<RcreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RcreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RcreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
