import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
import { DiscardconfirmationComponent } from '../discardconfirmation/discardconfirmation.component';
import { User } from '../Models/user.model';
import { UserService } from '../user.service';
interface Contact {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {
  userForm: FormGroup;
  contactnumbers: FormArray;
  id: number = 1;
  checkContactType: string;
  formInvalidSubmit: boolean;
  users: User[];
  UserId: Number;
  userDataObj: User
  // userForm: FormGroup;
  // contactnumbers: FormArray;
  lastIndex: Number;
  createFormstatus: boolean;
  // checkContactType: string;

  constructor
    (private dataService: DataService, private activatedRoute: ActivatedRoute, private fb: FormBuilder,
      private snackBar: MatSnackBar, private router: Router, private userService: UserService,
      public dialog: MatDialog) {

  }




  ngOnInit(): void {
    this.intializeCreateForm()
    //To maintain add remove icons for last input values
    this.contactnumbers = this.userForm.get('contactnumbers') as FormArray;
    this.lastIndex = this.contactnumbers.value.length - 1
    this.activatedRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.UserId = parseInt(data.get('id'))
        this.getUsersData()
        this.intializeUpdateForm()
        this.assiginingContactsToForm()
      }
      // console.log(data);
      //To maintain add remove icons for last input values
      // this.contactnumbers = this.userForm.get('contactnumbers') as FormArray;
      // this.lastIndex = this.contactnumbers.value.length - 1
    })
  }


  contactTypes: Contact[] = [
    { value: 'mobile', viewValue: 'Mobile' },
    { value: 'landline', viewValue: 'Landline' },
  ];

  intializeCreateForm() {
    this.createFormstatus = true
    this.userForm = this.fb.group({
      favourite: [false],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      gender: ['', Validators.required],
      martialstatus: [null, Validators.required],
      id: [''],
      contactnumbers: this.fb.array([
        this.createContactArray()
      ])
    })
  }
  intializeUpdateForm() {
    this.createFormstatus = false
    this.userForm = this.fb.group({
      favourite: [this.userDataObj?.favourite],
      firstname: [this.userDataObj?.firstname, Validators.required],
      lastname: [this.userDataObj?.lastname, Validators.required],
      gender: [this.userDataObj?.gender, Validators.required],
      martialstatus: [this.userDataObj?.martialstatus, Validators.required],
      id: [this.userDataObj?.id],
      contactnumbers: this.fb.array([
      ])


    })

    this.dataService.getNavigateStatus().subscribe((data) => {
      if (data=="createuser") {
        this.dirty(data)
      }
    })
  }
  // createContactArray(): FormGroup {
  //   return this.fb.group({
  //     contact: ['', Validators.required],
  //     number: ['', Validators.required]
  //   });
  // }
  createContactArray(): FormGroup {
    return this.fb.group({
      contact: ['', Validators.required],
      number: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')]]
    });
  }


  getId() {
    let id = this.userService.createId()
    if (!id) {
      console.log(id);
      this.userForm.get('id').setValue(this.id)
    } else {
      console.log(id);
      this.userForm.get('id').setValue(id)
    }
  }

  createUser(userForm, formDirective) {
    if (userForm.valid) {
      this.getId()
      this.userService.createUsersList(userForm.value).subscribe((response) => {
        if (response.success) {
          this.snackBar.open("user Created Succesfully", "close", { duration: 3000 });
          this.dataService.setRefreshData(true)
          // this.formInvalidSubmit = false
          this.resetForm()
          formDirective.resetForm()
        } else {
          this.snackBar.open("user Creation Failed", "close", { duration: 3000 });
        }
      })
    }
    else {
      this.formInvalidSubmit = true
      this.snackBar.open("Please fill all the Details", "close", { duration: 3000 });
    }
  }



  getUsersData() {
    this.userService.getUsersList().subscribe((data) => {
      this.users = data
      this.edit()
    })
  }

  edit() {
    console.log("edit calling");
    this.userDataObj = this.users.find(element => element.id == this.UserId)
    if (!this.userDataObj) {
      // console.log("didn't matched with any id");
      this.dataService.setRefreshData("error");
    }
    console.log(this.userDataObj);
  }

  update(userForm) {
    if (userForm.valid) {
      var length = 0
      this.users.forEach(x => {
        length++
        if (x['id'] == this.UserId) {
          this.users.splice(length - 1, 1, userForm.value)
          this.userService.updateUsersList(this.users)
        }
        this.snackBar.open("User updated succesfully", "close", {
          duration: 3000
        });
      })
      this.dataService.setRefreshData(true)
    } else {
      this.snackBar.open("Please fill all the Details", "close", { duration: 3000 });
    }
  }





  deleteUser() {
    var length = 0
    this.users.forEach(x => {
      length++
      if (x['id'] == this.UserId) {
        this.users.splice(length - 1, 1)
        this.userService.deleteUsersList(this.users)
        this.router.navigate(['createform'])
      }
      this.snackBar.open("User Deleted Succesfully", "close", {
        duration: 3000
      });
    })
    this.dataService.setRefreshData(true)
  }

  addContactRow() {
    this.contactnumbers = this.userForm.get('contactnumbers') as FormArray;
    this.contactnumbers.push(this.createContactArray());
    this.lastIndex = this.contactnumbers.value.length - 1
  }


  removeContactRow(i) {
    this.contactnumbers.removeAt(i);
    this.lastIndex = this.contactnumbers.value.length - 1
  }


  get contacts(): FormArray {
    return this.userForm.get('contactnumbers') as FormArray;
  }

  ChangeContactTypeValidation(i) {

    this.checkContactType = this.userForm.get('contactnumbers')['controls'][i]['controls']['contact'].value
    if (this.checkContactType == "mobile") {
      this.contacts['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')])
      this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
    } else if (this.checkContactType == "landline") {
      this.contacts['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.minLength(11), Validators.maxLength(11)])
      this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
    }

  }

  resetForm() {
    this.userForm.reset();
    // this.ngOnInit()
  }



  dirty(component) {
    console.log("dirty called", this.userForm.dirty);

    if (this.userForm.dirty) {
      let dialogRef = this.dialog.open(DiscardconfirmationComponent, {
        height: '200px',
        width: '400px',
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log("popup");

        console.log(`Dialog result: ${result}`); // Pizza!
        if (result == "save") {

          this.update(this.userForm)
          // this.dataService.setNavigateStatus(false)
          this.router.navigate([component])
        }
        else if (result == "discard") {
          // this.dataService.setNavigateStatus(true)
          this.router.navigate([component])
        }else
        {
          return;
        }
      });

    } else {
      // this.dataService.setNavigateStatus(false)
      this.router.navigate([component])
    }

  }




























  assiginingContactsToForm() {
    this.userDataObj?.contactnumbers.forEach((element, index) => {
      this.contacts.push(
        this.fb.group({
          contact: [element.contact, Validators.required],
          number: [element.number, Validators.required]
        })
      )
      this.ChangeContactTypeValidation(index)
    });
  }








  // get contacts(): FormArray {
  //   return this.userForm.get('contactnumbers') as FormArray;
  // }

  // ChangeContactTypeValidation(i) {

  //   this.checkContactType = this.contacts['controls'][i]['controls']['contact'].value
  //   console.log(this.checkContactType);

  //   if (this.checkContactType == "mobile") {
  //     this.contacts['controls'][i]['controls']['number']
  //       .setValidators([Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')])
  //     this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
  //   } else if (this.checkContactType == "landline") {
  //     this.contacts['controls'][i]['controls']['number']
  //       .setValidators([Validators.required, Validators.minLength(11)])
  //     this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
  //   }
  // }

}
