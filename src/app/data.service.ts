import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

    subject = new Subject();

   setRefreshData(isRefresh)
   {
     this.subject.next(isRefresh)
   }
    getRefreshData():Observable<any> 
   {
     return  this.subject.asObservable()
   }
   
   setNavigateStatus(isRefresh)
   {
     this.subject.next(isRefresh)
   }
    getNavigateStatus():Observable<any> 
   {
     return  this.subject.asObservable()
   }


}
