import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from '../data.service';
import { User } from '../Models/user.model';
import { UserService } from '../user.service';
import { TranslateService } from '@ngx-translate/core';
import libphonenumber from 'google-libphonenumber';
// const AsYouTypeFormatter = require('google-libphonenumber').AsYouTypeFormatter;
import { parse, format, AsYouType } from 'google-libphonenumber';
import { validateHorizontalPosition } from '@angular/cdk/overlay';
import { PhonePipe } from '../phone.pipe';



interface Contact {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-form-create',
  templateUrl: './form-create.component.html',
  styleUrls: ['./form-create.component.css']
})
export class FormCreateComponent implements OnInit {

  lastIndex: Number;
  // users: User[];
  createForm: FormGroup;
  contactnumbers: FormArray;
  id: number = 1;
  checkContactType: string;
  formInvalidSubmit: boolean;


  constructor(private dataService: DataService, private fb: FormBuilder,
    private snackBar: MatSnackBar, private userService: UserService, private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.intializeForm()
    //To maintain add remove icons for last input values
    this.contactnumbers = this.createForm.get('contactnumbers') as FormArray;
    this.lastIndex = this.contactnumbers.value.length - 1
  }

  contactTypes: Contact[] = [
    { value: 'mobile', viewValue: 'Mobile' },
    { value: 'landline', viewValue: 'Landline' },
  ];

  intializeForm() {
    this.createForm = this.fb.group({
      favourite: [false],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      gender: ['', Validators.required],
      martialstatus: [null, Validators.required],
      id: [''],
      contactnumbers: this.fb.array([
        this.createContactArray()
      ])
    })
  }

  createContactArray(): FormGroup {
    return this.fb.group({
      contact: ['', Validators.required],
      number: ['', Validators.required]
    });
  }


  getId() {
    let id = this.userService.createId()
    if (!id) {
      console.log(id);
       this.createForm.get('id').setValue(this.id)
    } else {
      console.log(id);
      this.createForm.get('id').setValue(id)
    }
  }

  createUser(createForm, formDirective) {
    if (createForm.valid) {
      this.getId()
      this.userService.createUsersList(createForm.value).subscribe((response) => {
        if (response.success) {
          this.snackBar.open("user Created Succesfully", "close", { duration: 3000 });
          this.dataService.setRefreshData(true)
          // this.formInvalidSubmit = false
          this.resetForm()
          formDirective.resetForm()
        } else {
          this.snackBar.open("user Creation Failed", "close", { duration: 3000 });
        }
      })
    }
    else {
      this.formInvalidSubmit = true
      this.snackBar.open("Please fill all the Details", "close", { duration: 3000 });
    }
  }


  addContactRow() {
    this.contactnumbers = this.createForm.get('contactnumbers') as FormArray;
    this.contactnumbers.push(this.createContactArray());
    this.lastIndex = this.contactnumbers.value.length - 1
  }

  removeContactRow(i) {
    this.contactnumbers.removeAt(i);
    this.lastIndex = this.contactnumbers.value.length - 1
  }


  get contacts(): FormArray {
    return this.createForm.get('contactnumbers') as FormArray;
  }

  ChangeContactTypeValidation(i) {

    this.checkContactType = this.createForm.get('contactnumbers')['controls'][i]['controls']['contact'].value
    if (this.checkContactType == "mobile") {
      this.contacts['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')])
      this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
    } else if (this.checkContactType == "landline") {
      this.contacts['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.minLength(11), Validators.maxLength(11)])
      this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
    }

  }

  resetForm() {
    this.createForm.reset();
    // this.ngOnInit()
  }





}
