import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl  } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { User } from '../Models/user.model';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
public contactNumbers:any[]=[{contact_type:'',number:''}];
lastIndex:Number;
user:User[]
id:number=1;
retrievedArray:User[]



  constructor(private dataService:DataService,private fb: FormBuilder) { }


  ngOnInit(): void {
    //To maintain add remove icons for last input values
    this.lastIndex=this.contactNumbers.length-1
  }



  addNumber()
  {
      this.contactNumbers.push({
        contact_type:'',
        number:'',
      });
    
      //To maintain add remove icons for last input values
      this.lastIndex=this.contactNumbers.length-1    
  }

  removeNumber(i)
  {
      this.contactNumbers.splice(i, 1);
      //To maintain add remove icons for last input values
      this.lastIndex=this.contactNumbers.length-1
  }

  create(value)
  {
    
    this.retrievedArray =  JSON.parse(localStorage.getItem('users'));
    if(this.retrievedArray==null)
    {
    value.id=1
    value.contactNumbers=this.contactNumbers
   this.user.push(value)
   localStorage.setItem('users', JSON.stringify(this.user));
    }else{
    var LastId=this.retrievedArray[this.retrievedArray.length-1]['id']
    value.id=LastId+1
    value.contactNumbers=this.contactNumbers
    this.retrievedArray.push(value)
    console.log(this.retrievedArray);
    localStorage.setItem('users', JSON.stringify(this.retrievedArray));
    }
this.dataService.setRefreshData(true)
  }
}
