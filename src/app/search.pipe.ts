import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(usersArray: object[], searchTerm: string): any {


    if (searchTerm==undefined) {
      return usersArray
      
    }
  
const searchArr = [];
if (searchTerm) {
  for (let i = 0; i < usersArray.length; i++) {
    console.log(usersArray[i]['firstname'].search(searchTerm));
    const searchIndex = (usersArray[i]['firstname']).toLowerCase().search(searchTerm.toLowerCase());
    if (searchIndex !== -1) {
      const obj = {
        index: searchIndex,
        ...usersArray[i]
      };
      console.log(obj);
      
      searchArr.push(obj);
      
    }
  }
  searchArr.sort((a, b) => a.index - b.index);
  console.log(searchArr);
  return searchArr;

}else{
  return usersArray
}

}

  
}

