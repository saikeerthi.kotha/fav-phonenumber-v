import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
import { User } from '../Models/user.model';
import { UserService } from '../user.service';
interface Contact {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.css']
})
export class FormEditComponent implements OnInit {
  users: User[];
  UserId: Number;
  userDataObj: User
  updateForm: FormGroup;
  contactnumbers: FormArray;
  lastIndex: Number;
  checkContactType: string;


  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute, private fb: FormBuilder,
    private snackBar: MatSnackBar, private router: Router, private userService: UserService) {


  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((data) => {
      // console.log(data);
      this.UserId = parseInt(data.get('id'))
      this.getUsersData()
      this.intializeForm()
      this.assiginingContactsToForm()
      //To maintain add remove icons for last input values
      this.contactnumbers = this.updateForm.get('contactnumbers') as FormArray;
      this.lastIndex = this.contactnumbers.value.length - 1


    })

    // this.dataService.getRefreshData().subscribe((response) => {
    //   if (response) {
    //     this.getUsersData()
    //     this.edit()
    //     this.intializeForm()
    //     this.contactnumbers = this.updateForm.get('contactnumbers') as FormArray;
    //     console.log(this.contactnumbers.value);
    //     this.assiginingContactsToForm()

    //     //To maintain add remove icons for last input values
    //     this.lastIndex = this.contactnumbers.value.length - 1
    //   }

    // })
    // this.getUsersData()
    // this.intializeForm()
    // this.assiginingContactsToForm()

    // //To maintain add remove icons for last input values
    // this.contactnumbers = this.updateForm.get('contactnumbers') as FormArray;
    // this.lastIndex = this.contactnumbers.value.length - 1
  }


  contactTypes: Contact[] = [
    { value: 'mobile', viewValue: 'Mobile' },
    { value: 'landline', viewValue: 'Landline' },
  ];
  intializeForm() {

    this.updateForm = this.fb.group({
      favourite: [this.userDataObj['favourite']],
      firstname: [this.userDataObj['firstname'], Validators.required],
      lastname: [this.userDataObj['lastname'], Validators.required],
      gender: [this.userDataObj['gender'], Validators.required],
      martialstatus: [this.userDataObj['martialstatus'], Validators.required],
      id: [this.userDataObj['id']],
      contactnumbers: this.fb.array([
      ])


    })
  }

  createContactArray(): FormGroup {
    return this.fb.group({
      contact: ['', Validators.required],
      number: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')]]
    });
  }

  getUsersData() {
    this.userService.getUsersList().subscribe((data) => {
      this.users = data
      this.edit()
    })
  }

  edit() {
    console.log("edit calling");
    this.userDataObj = this.users.find(element => element.id == this.UserId)
    console.log(this.userDataObj);
  }


  assiginingContactsToForm() {
    this.userDataObj['contactnumbers'].forEach((element, index) => {
      this.contacts.push(
        this.fb.group({
          contact: [element.contact, Validators.required],
          number: [element.number, Validators.required]
        })
      )
      this.ChangeContactTypeValidation(index)
    });
  }


  addContactRow() {
    this.contactnumbers = this.updateForm.get('contactnumbers') as FormArray;
    this.contactnumbers.push(this.createContactArray());
    this.lastIndex = this.contactnumbers.value.length - 1
  }

  removeContactRow(i) {
    this.contactnumbers.removeAt(i);
    // this.contactnumbers['value'].splice(i, 1);
    this.lastIndex = this.contactnumbers.value.length - 1
  }

  update(updateForm) {
    if (updateForm.valid) {
      var length = 0
      this.users.forEach(x => {
        length++
        if (x['id'] == this.UserId) {
          this.users.splice(length - 1, 1, updateForm.value)
          this.userService.updateUsersList(this.users)
        }
        this.snackBar.open("User updated succesfully", "close", {
          duration: 3000
        });
      })
      this.dataService.setRefreshData(true)
    } else {
      this.snackBar.open("Please fill all the Details", "close", { duration: 3000 });
    }
  }


  deleteUser() {
    var length = 0
    this.users.forEach(x => {
      length++
      if (x['id'] == this.UserId) {
        this.users.splice(length - 1, 1)
        this.userService.deleteUsersList(this.users)
        this.router.navigate(['createform'])
      }
      this.snackBar.open("User Deleted Succesfully", "close", {
        duration: 3000
      });
    })
    this.dataService.setRefreshData(true)
  }

  get contacts(): FormArray {
    return this.updateForm.get('contactnumbers') as FormArray;
  }

  ChangeContactTypeValidation(i) {

    this.checkContactType = this.contacts['controls'][i]['controls']['contact'].value
    console.log(this.checkContactType);

    if (this.checkContactType == "mobile") {
      this.contacts['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')])
      this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
    } else if (this.checkContactType == "landline") {
      this.contacts['controls'][i]['controls']['number']
        .setValidators([Validators.required, Validators.minLength(11)])
      this.contacts['controls'][i]['controls']['number'].updateValueAndValidity();
    }
  }


}
