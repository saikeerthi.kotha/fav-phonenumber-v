import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { User } from '../Models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
  retrievedArray:object[]
  searchTerm:String;
  constructor(private router:Router, private dataservice:DataService, private userService:UserService) { }

  ngOnInit(): void {
    this.dataservice.getRefreshData().subscribe((response)=>{
      if(response)
      {
        this.userService.getUsersList().subscribe((data)=>{
          this.retrievedArray =  data
        })
      }
    })
    this.userService.getUsersList().subscribe((data)=>{
      this.retrievedArray =  data
    })
  }

  navigateToEdit(id)
  {
    // this.dataservice.setRefreshData(true)
   this.router.navigate([`delete/${id}`])  
  }
  navigateToCreate()
  {
    this.router.navigate([`create`])  
  }
}
