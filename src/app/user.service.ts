import { Injectable } from '@angular/core';
import { User, UserAdaptor } from './Models/user.model';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // users:object[];
  constructor(private adapter: UserAdaptor) {

  }

  createId() {
    let users = JSON.parse(localStorage.getItem('users'))
    if (users == undefined || users.length == 0) {
      return;
    }
    return users[users.length - 1].id+1
  }

  createUsersList(user): Observable<any> {
    let usersArray = JSON.parse(localStorage.getItem('users'))
    if (!usersArray) {
      usersArray = []
    }
    usersArray.push(user)
    localStorage.setItem('users', JSON.stringify(usersArray))
    return of({ success: true })
  }
  updateUsersList(users) {
    localStorage.setItem('users', JSON.stringify(users))
  }
  deleteUsersList(users) {
    localStorage.setItem('users', JSON.stringify(users))
  }

  getUsersList(): Observable<User[]> {
    let users = JSON.parse(localStorage.getItem('users'))
    console.log(users);
    // if (users) {
    return of(users?.map((data: User[]) => this.adapter.adapt(data)))

    // }
  }



}
