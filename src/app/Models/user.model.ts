import { Injectable } from "@angular/core";
import { Adapter } from "./adapter";

// export class User{
//     constructor(
//    public  favourite:string,
//    public firstName:string,
//    public  lastName:string,
//    public gender:string,
//    public martialStatus:string,
//    public contactnumbers:ContactNumber[]){

//     }
export class User {
  constructor() { }
  public favourite: boolean;
  public firstname: string;
  public lastname: string;
  public gender: string;
  public martialstatus: string;
  public id: number;
  public contactnumbers?: ContactNumber[] = [];

}
export class ContactNumber {
  contact: string;
  number: string;
}


@Injectable({
  providedIn: 'root'
})
export class UserAdaptor implements Adapter<User> {
  adapt(item: any): User {
    // console.log(item);

    let user = new User()
    user.firstname = item.firstname ? item.firstname : null
    user.lastname = item.lastname ? item.lastname : null
    user.favourite = item.favourite ? item.favourite : null
    user.gender = item.gender ? item.gender : null
    user.martialstatus = item.martialstatus ? item.martialstatus : null
    user.id = item.id ? item.id : null
    user.contactnumbers = item.contactnumbers ? this.getContactNumbers(item.contactnumbers) : null

    return user
  }

  getContactNumbers(contactInfo: any[]) {
    return contactInfo.map((contactInfo:ContactNumber[]) => new ContactNumberAdapter().adapt(contactInfo))
  }
}

export class ContactNumberAdapter implements Adapter<ContactNumber>
{
  adapt(item: any) {
    let contactnumbers = new ContactNumber
    contactnumbers.contact = item.contact ? item.contact : null
    contactnumbers.number = item.number ? item.number : null
    return contactnumbers;
  }
}
