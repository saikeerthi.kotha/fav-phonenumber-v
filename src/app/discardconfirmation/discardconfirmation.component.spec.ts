import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscardconfirmationComponent } from './discardconfirmation.component';

describe('DiscardconfirmationComponent', () => {
  let component: DiscardconfirmationComponent;
  let fixture: ComponentFixture<DiscardconfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiscardconfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscardconfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
