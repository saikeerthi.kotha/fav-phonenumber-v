import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-discardconfirmation',
  templateUrl: './discardconfirmation.component.html',
  styleUrls: ['./discardconfirmation.component.css']
})
export class DiscardconfirmationComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DiscardconfirmationComponent> ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
  }

  discard(selection)
  {
    this.dialogRef.close(selection);
  }
  onNoClick()
  {
    this.dialogRef.close();

  }
}
