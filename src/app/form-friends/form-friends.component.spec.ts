import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFriendsComponent } from './form-friends.component';

describe('FormFriendsComponent', () => {
  let component: FormFriendsComponent;
  let fixture: ComponentFixture<FormFriendsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormFriendsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
