import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { User } from '../Models/user.model';
import { UserService } from '../user.service';
@Component({
  selector: 'app-form-friends',
  templateUrl: './form-friends.component.html',
  styleUrls: ['./form-friends.component.css']
})
export class FormFriendsComponent implements OnInit {
  searchTerm: string;
  usersList: User[]
  constructor(private router: Router, private dataService: DataService, private userService: UserService) { }

  ngOnInit(): void {
    this.dataService.getRefreshData().subscribe((response) => {
      if (response) {
        this.userService?.getUsersList().subscribe((data) => {
          this.usersList = data?.reverse()
        })
      }
    })
    this.userService?.getUsersList().subscribe((data) => {
      this.usersList = data?.reverse()
    })
  }

  navigateToEdit(id) {
    console.log("id", id);
    this.router.navigate([`edituser/${id}`])
    // this.dataService.setRefreshData(true)

  }

  
  navigateToCreate() {
    console.log("calling nav");
    this.dataService.setNavigateStatus('createuser')
    //     this.dataService.getNavigateStatus().subscribe((data)=>{
    // console.log(data);

    //       if(!data)
    //       {
    //         this.router.navigate([`createuser`])
    //       }else
    //       {
    //         return;
    //       }
    //     })
  }
}
