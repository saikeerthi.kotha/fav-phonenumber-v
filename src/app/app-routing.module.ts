import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { DeleteComponent } from './delete/delete.component';
import { FormCreateComponent } from './form-create/form-create.component';
import { FormEditComponent } from './form-edit/form-edit.component';
import { FormFriendsComponent } from './form-friends/form-friends.component';
import { UserformComponent } from './userform/userform.component';


const routes: Routes = [{ path: '', redirectTo: 'createuser', pathMatch: 'full' },
// { path: 'create', component: CreateComponent },
// { path: 'delete/:id', component: DeleteComponent },
// { path: 'customers', loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule) },
// { path: 'createuser', component: FormCreateComponent },
// { path: 'edit/:id', component: FormEditComponent },
// { path: 'friendsform', component: FormFriendsComponent },
{ path: 'createuser', component: UserformComponent },
{ path: 'edituser/:id', component: UserformComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
