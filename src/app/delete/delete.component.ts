import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  retrievedArray: object[];
  lastIndex: Number;
  contactNumbers: object[];
  editData: any[];
  UserId: string;
  userData: object[];
  constructor(private _activatedRoute: ActivatedRoute, private _dataService: DataService) { }

  ngOnInit(): void {
    this._dataService.getRefreshData().subscribe((response) => {
      if (response) {
        this.edit()
        // this.lastIndex = this.contactNumbers.length - 1

      }
    })
    this.edit()
    //To maintain add remove icons for last input values
    // this.lastIndex = this.contactNumbers.length - 1
  }

  edit() {
    this.editData = this.retrievedArray = JSON.parse(localStorage.getItem('users'));

    // this.contactNumbers=this.editData.contactNumbers
    this._activatedRoute.paramMap.subscribe((data) => {
      this.UserId = data.get('id')
    })

    this.userData = this.editData.filter(x => {
      if (x['id'] == this.UserId) {
        return x
      }
    })
    this.contactNumbers = this.userData[0]['contactNumbers']
  }

  addnumber() {
    console.log("vasanth");
    this.contactNumbers.push({
      contact_type: '',
      number: '',
    });
    //To maintain add remove icons for last input values
    this.lastIndex = this.contactNumbers.length - 1
    console.log(this.contactNumbers.length);
  }

  removenumber(i) {
    this.contactNumbers.splice(i, 1);
    //To maintain add remove icons for last input values
    this.lastIndex = this.contactNumbers.length - 1

  }

  update(value) {
    this.retrievedArray = JSON.parse(localStorage.getItem('users'));
    value.contactNumbers = this.contactNumbers
    var count = 0
    this.retrievedArray.forEach(x => {
      count++
      if (x['id'] == this.UserId) {
        // value.id = this.UserId
        this.retrievedArray.splice(count - 1, 1, value)
        
        localStorage.setItem('users', JSON.stringify(this.retrievedArray));
      }
    })
    this._dataService.setRefreshData(true)

  }


  deleteUser(id, index) {
    this.retrievedArray = JSON.parse(localStorage.getItem('users'));
    this.retrievedArray.forEach(x => {
      if (x['id'] == id) {
        this.retrievedArray.splice(index, 1)
        localStorage.setItem('users', JSON.stringify(this.retrievedArray));
      }
    })
    this._dataService.setRefreshData(true)
  }

}
