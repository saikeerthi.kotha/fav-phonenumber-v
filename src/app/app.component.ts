import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'vasanth';
  showError:boolean;
  constructor(public translate: TranslateService,private dataService: DataService)
  {
    translate.setDefaultLang('en');
    translate.addLangs(['en','fr'])
    const browserLang=translate.getBrowserLang()
    translate.use(browserLang.match(/en|fr/)?browserLang: 'en')
    // this.getTransLanguage()

    this.dataService.getRefreshData().subscribe((data)=>{
      console.log(data);
      if(data=="error"){
        this.showError=true
      }
    })
  }
  // getLangs()
  // {
  //   return  this.translate.getLangs()
  // }

  switchLanguage(language: string) { 
    this.translate.use(language);
  }

  // getTransLanguage() {
  //   this.TransLang = [...this.translate.getLangs()];
  // }

  // switchLanguage() {
  //   // console.log("lang", language);
  //   this.translate.use(this.selectLang);
  // }
}
